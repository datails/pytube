package main

import (
	"github.com/wailsapp/wails/v2/pkg/runtime"
)

func (a *App) SelectDir() (string, error) {
	dir, err := runtime.OpenDirectoryDialog(a.ctx, runtime.OpenDialogOptions{
		Title: "Select directory",
	})

	if err != nil {
		return "", err
	}

	return dir, nil
}

func (a *App) SelectFile() (string, error) {
	options := runtime.OpenDialogOptions{
		Title: "Select File",
		Filters: []runtime.FileFilter{
			{
				DisplayName: "Select files",
				Pattern:     "*.mp3;*.mp4;*.webm;*.mkv;*.avi;*.mov;*.flv;*.wmv;*.m4a;*.m4v;*.mpg;*.mpeg;*.m2v;*.3gp;*.3g2;*.ogg;*.ogv;*.oga;*.ogx;*.opus;*.spx;*.oga;*.ogx;*.opus;*.spx;*.oga;*.ogx;*.opus;*.spx;*.flac;*.wav;*.wma;*.aac;*.ac3;*.dts;*.alac;*.amr;*.ape;*.apl;*.mac;*.mpc;*.ofr;*.ofs;*.ogg;*.spx;*.tta;*.wv;*.wma;*.wv;*.mp3;*.mp4;*.webm;*.mkv;*.avi;*.mov;*.flv;*.wmv;*.m4a;*.m4v;*.mpg;*.mpeg;*.m2v;*.3gp;*.3g2;*.ogg;*.ogv;*.oga;*.ogx;*.opus;*.spx;*.oga;*.ogx;*.opus;*.spx;*.oga;*.ogx;*.opus;*.spx;*.flac;*.wav;*.wma;*.aac;*.ac3;*.dts;*.alac;*.amr;*.ape;*.apl;*.mac;*.mpc;*.ofr;*.ofs;*.ogg;*.spx;*.tta;*.wv;*.wv;*.mp3;*.mp4;*.webm;*.mkv;*.avi;*.mov;*.flv;*.wmv;*.m4a;*.m4v;*.mpg;*.mpeg;*.m2v;*.3gp;*.3g2;*.ogg;*.ogv;*.oga;*.ogx;*.opus;*.spx;*.oga;*.ogx;*.opus;*.spx;*.oga;*.ogx;*.opus;*.spx;*.flac;*.wav;*.wma;*.aac;*.ac3;*.dts;*.alac;*.amr;*.ape;*.apl;*.mac",
			},
		},
	}

	filename, err := runtime.OpenFileDialog(a.ctx, options)
	if err != nil {
		return "", err
	}

	return filename, nil
}
