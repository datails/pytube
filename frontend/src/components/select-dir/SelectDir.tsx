import { FormControl, TextField } from '@mui/material'
import { SelectDir } from '@wails/go/main/App'
import withRoot from '@/theme/withRoot'

interface Props {
  label?: string
  directory: string
  setDirectory: (directory: string) => void
}

function SelectDirectory({ label, directory, setDirectory }: Props) {
  function selectFile() {
    SelectDir()
      .then((directory) =>
        setDirectory(directory),
      )
      .catch(console.error)
  }

  return (
    <FormControl className='form-control'>
      <TextField
        value={directory}
        onClick={selectFile}
        label={ label || 'Select a target directory.'}
        id='directory'
        variant='outlined'
      />
    </FormControl>
  )
}

export default withRoot(SelectDirectory)
