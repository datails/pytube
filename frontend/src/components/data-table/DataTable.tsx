import { DataGrid, GridColDef } from '@mui/x-data-grid'
import withRoot from '@/theme/withRoot'

interface Props {
  rows: any[]
  columns: GridColDef[]
}

function DataTable({ columns, rows }: Props) {
  return (
    <div style={{ width: '100%' }}>
      <DataGrid
        initialState={{
          sorting: {
            sortModel: [{ field: 'createdAt', sort: 'asc' }],
          }, columns: { columnVisibilityModel: { delimiter: false } } }}
        autoHeight
        rows={rows}
        columns={columns}
        checkboxSelection
      />
    </div>
  )
}

export default withRoot(DataTable)
