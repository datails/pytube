import { FormControl, TextField } from '@mui/material'
import { SelectFile as SelectWailsFile } from '@wails/go/main/App'
import withRoot from '@/theme/withRoot'

interface Props {
  label?: string
  filePath: string
  setFilePath: (directory: string) => void
}

function SelectFile({ label, filePath, setFilePath }: Props) {
  function selectFile() {
    SelectWailsFile()
      .then((filePath) =>
        setFilePath(filePath),
      )
      .catch(console.error)
  }

  return (
    <FormControl className='form-control'>
      <TextField
        value={filePath}
        onClick={selectFile}
        label={ label || 'Select a file.'}
        id='file'
        variant='outlined'
      />
    </FormControl>
  )
}

export default withRoot(SelectFile)
