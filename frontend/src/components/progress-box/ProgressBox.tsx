import { CircularProgress } from '@mui/material'
import { Box } from '@mui/system'
import withRoot from '@/theme/withRoot'

function ProgressBox() {
  return (
    <Box sx={{ display: 'flex' }}>
      <CircularProgress />
    </Box>
  )
}

export default withRoot(ProgressBox)
