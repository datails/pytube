import React from 'react'
import { DEFAULT_DATA, StepperData } from './stepper.context'
import useInterval from '@/hooks/useInterval'
import { Prefix } from '@/constants/prefixes'
import { List } from '@wails/go/main/App'
import { parse } from '@/utils'

interface Settings {
  directory: string
}

export const AppContext = React.createContext({
  loading: false,
  setLoading: (_bool: boolean) => {},
})

export const SettingsContext = React.createContext({
  settings: {} as Partial<Settings>,
  setSettings: (_settings: Partial<Settings>) => {},
})

export const AssetSubmitContext = React.createContext({
  data: DEFAULT_DATA,
  setData: (_data: StepperData) => {},
  open: false,
  setOpen: (_bool: boolean) => {},
})

export const QueueContext = React.createContext({
  queue: [] as any[],
  setQueue: (_queue: any[]) => {},
})

export function AppContextProvider({ children }: any) {
  const [loading, setLoading] = React.useState(false)
  const [settings, setSettings] = React.useState({})
  const [data, setData] = React.useState(DEFAULT_DATA)
  const [open, setOpen] = React.useState(false)
  const [queue, setQueue] = React.useState([] as any[])

  useInterval(async () => {
    const data = await List(Prefix.QUEUE)
    setQueue(data.map(parse))
  }, 3000)

  const queueValue = React.useMemo(
    () => ({
      queue,
      setQueue,
    }),
    [queue, setQueue],
  )

  const loadingValue = React.useMemo(
    () => ({
      loading,
      setLoading,
    }),
    [loading],
  )

  const settingsValue = React.useMemo(
    () => ({
      settings,
      setSettings,
    }),
    [settings, setSettings],
  )

  const assetSubmitValue = React.useMemo(
    () => ({
      data,
      setData,
      open,
      setOpen,
    }),
    [data, setData],
  )

  return (
    <AppContext.Provider value={loadingValue}>
      <SettingsContext.Provider value={settingsValue}>
        <AssetSubmitContext.Provider value={assetSubmitValue}>
          <QueueContext.Provider value={queueValue}>
            {children}
          </QueueContext.Provider>
        </AssetSubmitContext.Provider>
      </SettingsContext.Provider>
    </AppContext.Provider>
  )
}
