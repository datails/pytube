import React from 'react'

interface Meta {
  success: boolean
  error?: any
  data: {
    author: string
    title: string
    description: string
    image: string
    publish_date: string
    length: number
  }
}

interface Audio {
  success: boolean
  error?: any
  list: {
    itag: number
    mime_type: string
    abr: string
  }[]
}

interface Video {
  success: boolean
  error?: any
  list: {
    itag: number
    mime_type: string
    resolution: string
  }[]
}

interface Captions {
  success: boolean
  error?: any
  list: {
    code: string
    lang: string
  }[]
}
export interface StepperData {
  audio: Audio
  video: Video
  captions: Captions
  meta: Meta
}

export interface StepperQuery { 
  url: string
  audioItags: number[]
  videoItags: number[]
  captions: string[]
  useFFmpeg: boolean
}

export const DEFAULT_ERR_MSG = 'No data'
export const DEFAULT_QUERY = {
  url: '',
  audioItags: [],
  videoItags: [],
  captions: [],
  useFFmpeg: false,
} as StepperQuery

export const DEFAULT_DATA = {
  audio: {
    success: false,
    error: DEFAULT_ERR_MSG,
    list: [],
  },
  video: {
    success: false,
    error: DEFAULT_ERR_MSG,
    list: [],
  },
  captions: {
    success: false,
    error: DEFAULT_ERR_MSG,
    list: [],
  },
  meta: {
    success: false,
    error: DEFAULT_ERR_MSG,
    data: {
      author: '',
      title: '',
      description: '',
      image: '',
      publish_date: '',
      length: 0,
    },
  }
} as StepperData

export const stepperContext = React.createContext({
  query: {
    url: '',
    audioItags: [],
    videoItags: [],
    captions: [],
    useFFmpeg: false,
  } as StepperQuery,
  setQuery: (_query: StepperQuery) => {},
  data: {
    audio: {},
    video: {},
    captions: {},
    meta: {}
  } as StepperData,
  setData: (_data: StepperData) => {},
})

export function StepperContextProvider({ children }: any) {
  const [query, setQuery] = React.useState(DEFAULT_QUERY)
  const [data, setData] = React.useState(DEFAULT_DATA)

  const queryValue = React.useMemo(
    () => ({
      query,
      setQuery,
      data,
      setData,
    }),
    [data, setData, query, setQuery],
  )

  return (
    <stepperContext.Provider value={queryValue}>
      {children}
    </stepperContext.Provider>
  )
}