import React from "react";

export interface FFmpegContext {
  audioMeta: any
  videoMeta: any
  query: {
    audioFilePath: string
    videoFilePath: string
  }
}

export const ffmpegQueryContext = React.createContext({
  audioFilePath: "",
  videoFilePath: "",
  setAudioFilePath: (_path: string) => {},
  setVideoFilePath: (_path: string) => {},
})

export const ffmpegContext = React.createContext({
  audioMeta: {},
  setAudioMeta: (_meta: any) => {},
  videoMeta: {},
  setVideoMeta: (_meta: any) => {},
});

export function FFmpegContext({ children }: any) {
  const [audioMeta, setAudioMeta] = React.useState({} as any)
  const [videoMeta, setVideoMeta] = React.useState({} as any)
  const [audioFilePath, setAudioFilePath] = React.useState("")
  const [videoFilePath, setVideoFilePath] = React.useState("")

  const queryValue = React.useMemo(
    () => ({
      audioFilePath,
      videoFilePath,
      setAudioFilePath,
      setVideoFilePath,
    }),
    [audioFilePath, videoFilePath, setAudioFilePath, setVideoFilePath],
  )

  const metaValue = React.useMemo(
    () => ({
      audioMeta,
      videoMeta,
      setAudioMeta,
      setVideoMeta,
    }),
    [audioMeta, videoMeta, setAudioMeta, setVideoMeta],
  )

  return (
    <ffmpegContext.Provider value={metaValue}>
      <ffmpegQueryContext.Provider value={queryValue}>
        {children}
      </ffmpegQueryContext.Provider>
    </ffmpegContext.Provider>);
}