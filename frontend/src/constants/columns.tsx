import { IconButton } from '@mui/material'
import type { Function } from "ts-toolbelt"
import { GridColDef, GridValueFormatterParams } from '@mui/x-data-grid'
import DeleteIcon from '@mui/icons-material/Delete'
import { Delete } from '@wails/go/main/App';
import { QueueContext } from '@/context/app.context';
import { useContext } from 'react';

const onButtonClick = (e: any, row: any, setQueue: Function.Function) => {
  e.stopPropagation();  
  return Delete(row.id).then(() => setQueue((prev: any) => prev.filter((item: any) => item.id !== row.id)))
};

export const queueColumns: GridColDef[] = [
  {
    field: 'id',
    headerName: 'ID',
    description: 'ID of the download.',
  },
  {
    field: 'title',
    headerName: 'Asset Title',
    description: 'Title of the asset.',
    flex: 1,
  },
  {
    field: 'storagePath',
    headerName: 'Storage Path',
    description: 'Path to the asset.',
    flex: 1,
  },
  {
    field: 'type',
    headerName: 'Asset Type',
    description: 'Type of the asset',
    flex: 1,
  },
  {
    field: 'createdAt',
    headerName: 'Created at',
    description: 'Creation date.',
    valueFormatter: (params: GridValueFormatterParams) => new Date(params.value).toLocaleString(),
    flex: 1,
  },
  {
    field: 'status',
    description: 'Status of the download.',
    headerName: 'Status',
    width: 260,
    flex: 1,
  },
  {
    field: 'delete',
    headerName: 'Delete',
    description: 'Delete this formatting job.',
    // flex: 1,
    renderCell: (params) => {  
      const { setQueue } = useContext(QueueContext);
      
      return (
          <IconButton aria-label="delete" onClick={(e) => onButtonClick(e, params.row, setQueue)}>
            <DeleteIcon color="error" />
          </IconButton>
        );
      } 
  }
]
