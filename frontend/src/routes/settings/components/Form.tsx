import { useContext, useState } from 'react'
import { Button } from '@mui/material'
import { Set } from '@wails/go/main/App'
import { Stack } from '@mui/system'
import withRoot from '@/theme/withRoot'
import { Prefix } from '@/constants/prefixes'
import SelectDirectory from '@/components/select-dir/SelectDir'
import { SettingsContext } from '@/context/app.context'
import { useLoader } from '@/hooks/useLoader'

function Form() {
  const { setSettings, settings } = useContext(SettingsContext)
  const { execute } = useLoader(Set)

  const save = () => execute(
    Prefix.SETTINGS,
    JSON.stringify({
      ...settings,
      updatedAt: Date.now(),
      directory: settings.directory
    })
  )

  return (
    <>
      <SelectDirectory directory={settings.directory} setDirectory={(val: string) => setSettings({
        ...settings,
        directory: val
      })} />
      <Stack spacing={2} className='input-box'>
        <Button onClick={save} className='button form' variant='outlined'>
          Save
        </Button>
      </Stack>
    </>
  )
}

export default withRoot(Form)
