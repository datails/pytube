import { useState } from 'react'
import { Button } from '@mui/material'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import withRoot from '@/theme/withRoot'
import Dialog from './components/Dialog'
import { Reset } from '@wails/go/main/App'

function ResetAll() {
  const [open, setOpen] = useState(false)

  const handleOpen = () => {
    setOpen(true)
  }

  return (
    <>
      <Button variant='outlined' onClick={handleOpen} startIcon={<DeleteForeverIcon />}>
        Reset application
      </Button>
      <Dialog resetFn={Reset} open={open} setOpen={setOpen} />
    </>
  )
}

export default withRoot(ResetAll)
