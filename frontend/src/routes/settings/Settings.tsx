import { Card, CardActions, CardContent, Grid, Typography } from '@mui/material'
import withRoot from '@/theme/withRoot'
import Form from './components/Form'
import Reset from './components/reset/Reset'

function Settings() {
  return (
    <Grid container spacing={2} id='app'>
      <Grid item sm={6} flexDirection='column'>
        <Card>
          <CardContent>
            <Typography className='margin16 marginBottom32' gutterBottom variant='h1'>
              Settings
            </Typography>
            <Typography className='title marginBottom32' variant='h5'>
              Configure global application settings.
            </Typography>
            <Form />
          </CardContent>
        </Card>
      </Grid>
      <Grid item sm={6} flexDirection='column'>
        <Card>
          <CardContent>
            <Typography className='margin16 marginBottom32' gutterBottom variant='h1'>
              Reset
            </Typography>
            <Typography className='title marginBottom32' variant='h5'>
              Reset all configs, data and personal settings.
            </Typography>
          </CardContent>
          <CardActions className='padding16'>
            <Reset />
          </CardActions>
        </Card>
      </Grid>
    </Grid>
  )
}

export default withRoot(Settings)
