import withRoot from "@/theme/withRoot";
import { Grid, Typography } from "@mui/material";
import StepperForm from "./components/stepper-form/StepperForm";
import { FFmpegContext } from "@/context/ffmpeg.context";

function Synchronize(){
  return (
    <Grid container spacing={2} id="app">
      <Grid item xs={8} flexDirection="column" className="title-bar">
        <Typography className="margin16 marginBottom32" gutterBottom variant="h1">
          Synchronize
        </Typography>
        <Typography className="title marginBottom32" variant="h5">
          Perform audio dubbing by laying the audio file over a video file.
        </Typography>
        <FFmpegContext>
          <StepperForm />
        </FFmpegContext>
      </Grid>
    </Grid>
  )
}

export default withRoot(Synchronize)