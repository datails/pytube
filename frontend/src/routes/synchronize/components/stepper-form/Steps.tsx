import withRoot from "@/theme/withRoot"
import { Alert } from "@mui/material"
import SelectFiles from "./SelectFiles"

interface Props {
  step: number
}

function Steps({ step }: Props) {
  if (step === 0) { 
    return <SelectFiles />
  }

  return (
    <Alert severity="error" sx={{ width: '100%' }}>
      Unknown step
    </Alert>
  )
}

export default withRoot(Steps)