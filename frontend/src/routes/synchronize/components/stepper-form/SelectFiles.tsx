import { useCallback, useContext, useEffect, useState } from "react"
import type { Function } from "ts-toolbelt"
import SelectFile from "@/components/select-file/SelectFile"
import { ffmpegContext, ffmpegQueryContext } from "@/context/ffmpeg.context"
import withRoot from "@/theme/withRoot"
import { ExtractMetadata } from "@wails/go/main/App"
import { CircularProgress, Grid } from "@mui/material"

interface SelectSingleFileProps {
  filePath: string
  setFilePath: Function.Function
  setContext: Function.Function
  type: "audio" | "video"
}

function SelectSingleFile({ filePath, setFilePath, setContext, type }: SelectSingleFileProps) {
  const [ loading, setLoading ] = useState(false);

  const callbackFn = useCallback(async () => {
    if (filePath.length > 0) {
      const meta = await ExtractMetadata(filePath);
      setContext(JSON.parse(meta));
    }
  }, [filePath, setContext]);

  useEffect(() => {
    setLoading(true);
    callbackFn().finally(() => setLoading(false));
  }, [callbackFn]);

  return (
    <Grid container spacing={2} flexDirection="row">
      <Grid item xs={10}>
        <SelectFile label={`Select an ${type} file`} filePath={filePath} setFilePath={setFilePath} />
      </Grid>    
      <Grid item xs={2}>
        {loading && <CircularProgress />}
      </Grid>
    </Grid>
  );
}

function SelectFiles(){
  const { setAudioMeta, setVideoMeta } = useContext(ffmpegContext)
  const { audioFilePath, videoFilePath, setAudioFilePath, setVideoFilePath } = useContext(ffmpegQueryContext)

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <SelectSingleFile setFilePath={setAudioFilePath} filePath={audioFilePath} setContext={setAudioMeta} type="audio" />
      </Grid>
      <Grid item xs={12}>
        <SelectSingleFile setFilePath={setVideoFilePath} filePath={videoFilePath} setContext={setVideoMeta} type="video" />
      </Grid>
    </Grid>
  )
}

export default withRoot(SelectFiles)