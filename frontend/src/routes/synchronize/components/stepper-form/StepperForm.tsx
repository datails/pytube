import React, { useContext } from "react";
import {
  Step,
  Stepper,
  StepLabel,
  StepContent,
  Button,
  CardContent,
  Card,
  CardActions
} from "@mui/material";
import withRoot from "@/theme/withRoot";
import { getSteps } from "./steps-utils";
import Steps from "./Steps";
import { QueueContext } from "@/context/app.context";
import { ffmpegQueryContext } from "@/context/ffmpeg.context";
import { SetItem } from "@wails/go/main/App";
import { Prefix } from "@/constants/prefixes";
import { getParentDirectory } from "@/utils/sanitize";

export const createItem = (data: any) => ({
    id: Date.now().toString(),
    status: "waiting",
    createdAt: new Date().toISOString(),
    type: "ffmpeg",
    audioFile: data.audioFilePath,
    videoFile: data.videoFilePath,
    outputFile: `${getParentDirectory(data.videoFilePath)}/full.mp4`,
    storagePath: `${getParentDirectory(data.videoFilePath)}/full.mp4`,
    title: "ffmpeg",
  })

function FormStepper() {
  const [activeStep, setActiveStep] = React.useState(0);
  const { audioFilePath, setAudioFilePath, setVideoFilePath, videoFilePath } = useContext(ffmpegQueryContext);
  const { queue, setQueue } = React.useContext(QueueContext);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep(
      (prevActiveStep) => prevActiveStep + 1
    );
  };

  const handleSubmit = async () => {
    const item = createItem({ audioFilePath, videoFilePath });

    await SetItem(Prefix.QUEUE, JSON.stringify(item))

    setQueue([...queue, item])

    setActiveStep(
      (prevActiveStep) => prevActiveStep + 1
    );

    setTimeout(() => {
      setActiveStep(0)
      setAudioFilePath("")
      setVideoFilePath("")
    }, 500)
  }

  const handleBack = () => {
    setActiveStep(
      (prevActiveStep) => prevActiveStep - 1
    );
  };

  return (
    <>
      <Stepper
        activeStep={activeStep}
        orientation="vertical"
      >
        {steps.map((step, index) => (
          <Step>
            <StepLabel>{step.title}</StepLabel>
            <StepContent>
              <Card className="padding16 marginTopBottom16">
                <CardContent>
                  <Steps step={index} />
                </CardContent>
                <CardActions>
                  <Button
                    disabled={
                      activeStep === 0
                    }
                    onClick={handleBack}
                  >
                    Back
                  </Button>
                  <Button
                    disabled={!audioFilePath.length || !videoFilePath.length}
                    variant="contained"
                    color="primary"
                    onClick={activeStep ===
                      steps.length - 1
                      ? handleSubmit
                      : handleNext}
                  >
                    {activeStep ===
                      steps.length - 1
                      ? "Finish"
                      : "Next"}
                  </Button>
                </CardActions>
              </Card>
            </StepContent>
          </Step>
        ))}
      </Stepper>
    </>
  );
}

export default withRoot(FormStepper);