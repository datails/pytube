import * as React from 'react'
import { styled, useTheme } from '@mui/material/styles'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import List from '@mui/material/List'
import CssBaseline from '@mui/material/CssBaseline'
import Divider from '@mui/material/Divider'
import IconButton from '@mui/material/IconButton'
import MenuIcon from '@mui/icons-material/Menu'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'

import Logo from '@/assets/images/logo_transparent.png'

import Router from '../Routes'
import withRoot from '@/theme/withRoot'
import { Stack } from '@mui/system'
import { listItems } from './components/ListItems'
import { Drawer } from './components/Drawer'
import { DrawerHeader } from './components/DrawerHeader'
import { AppBar } from './components/AppBar'
import { Get } from '@wails/go/main/App'
import { Prefix } from '@/constants/prefixes'
import { AppContext, SettingsContext } from '@/context/app.context'

const AppLogo = styled('img')(({ theme }) => ({
  width: 64,
}))

export async function loadSettings(setSettings: (_settings: Record<string, unknown>) => void) {
  const defaultSettings = await Get(Prefix.SETTINGS).catch(console.error)
  setSettings(JSON.parse((defaultSettings as string) || '{}'))
}

function App() {
  const theme = useTheme()
  const [open, setOpen] = React.useState(true)
  const { setSettings } = React.useContext(SettingsContext)
  const { setLoading } = React.useContext(AppContext)

  const toggle = () => {
    setOpen(!open)
  }

  React.useEffect(() => {
    setLoading(true)
    loadSettings(setSettings).finally(() => setLoading(false))
  }, [setSettings])

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar position='fixed' open={open}>
        <Stack
          display='flex'
          flexDirection='row'
          component={Toolbar}
          justifyContent='space-between'
        >
          <IconButton
            color='inherit'
            aria-label='open drawer'
            onClick={toggle}
            edge='start'
            sx={{
              marginRight: 5,
              ...(open && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <AppLogo src={Logo} alt='Logo' id='logo' />
        </Stack>
      </AppBar>
      <Drawer variant='permanent' open={open}>
        <DrawerHeader>
          <IconButton onClick={toggle}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
          {listItems.map((item) => (
            <ListItem key={item.key} disablePadding sx={{ display: 'block' }}>
              <ListItemButton
                href={item.href}
                sx={{
                  minHeight: 48,
                  justifyContent: open ? 'initial' : 'center',
                  px: 2.5,
                }}
              >
                <ListItemIcon
                  sx={{
                    minWidth: 0,
                    mr: open ? 3 : 'auto',
                    justifyContent: 'center',
                  }}
                >
                  {item.icon}
                </ListItemIcon>
                <ListItemText primary={item.key} sx={{ opacity: open ? 1 : 0 }} />
              </ListItemButton>
            </ListItem>
          ))}
        </List>
        <Divider />
      </Drawer>
      <Box
        style={{
          padding: '0 24px',
          marginTop: '-16px',
        }}
        component='main'
        sx={{ flexGrow: 1, p: 3 }}
      >
        <DrawerHeader />
        <Router />
      </Box>
    </Box>
  )
}

export default withRoot(App)