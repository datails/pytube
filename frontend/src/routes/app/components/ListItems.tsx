import VideoFileIcon from '@mui/icons-material/VideoFile'
import SettingsIcon from '@mui/icons-material/Settings'
import SyncIcon from '@mui/icons-material/Sync';
import DownloadItemIcon from './DownloadItemIcon'
import PublishedWithChangesIcon from '@mui/icons-material/PublishedWithChanges';

export const listItems = [
  {
    key: 'Download assets',
    icon: <VideoFileIcon />,
    href: '/',
  },
  {
    key: 'Synchronize',
    icon: <SyncIcon />,
    href: '/synchronize',
  },
  // {
  //   key: 'Convert',
  //   icon: <PublishedWithChangesIcon />,
  //   href: '/convert',
  // },
  {
    key: 'Queue',
    icon: <DownloadItemIcon />,
    href: '/history',
  },
  {
    key: 'Settings',
    icon: <SettingsIcon />,
    href: '/settings',
  },
]
