import { useContext } from "react";
import { QueueContext } from "@/context/app.context";
import { Badge } from "@mui/material";
import HistoryIcon from '@mui/icons-material/History'
import withRoot from "@/theme/withRoot";

function DownloadItemIcon() {
  const { queue } = useContext(QueueContext)

  return (
    <Badge color="success" badgeContent={queue.filter(item => item.status === "waiting").length}>
      <HistoryIcon />
    </Badge>
  )
}

export default withRoot(DownloadItemIcon)