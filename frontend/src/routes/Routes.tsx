import { Routes, Route } from 'react-router-dom'
import History from './history/History'
import Settings from './settings/Settings'
import DownloadAssets from './download-assets/DownloadAssets'
import Synchronize from './synchronize/Synchronize'

const Router = () => {
  return (
    <>
      <Routes>
        <Route path='/' element={<DownloadAssets />}></Route>
        <Route path='/synchronize' element={<Synchronize />}></Route>
        <Route path='/history' element={<History />}></Route>
        <Route path='/settings' element={<Settings />}></Route>
      </Routes>
    </>
  )
}

export default Router
