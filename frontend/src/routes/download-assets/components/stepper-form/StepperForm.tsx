import React from "react";
import type { Function } from "ts-toolbelt";
import {
  Step,
  Stepper,
  StepLabel,
  StepContent,
  Button,
  CardContent,
  Card,
  CardActions
} from "@mui/material";
import withRoot from "@/theme/withRoot";
import { DEFAULT_DATA, StepperData, StepperQuery, stepperContext } from "@/context/stepper.context";
import { AssetSubmitContext, QueueContext, SettingsContext } from "@/context/app.context";
import { SetItem } from "@wails/go/main/App";
import { Prefix } from "@/constants/prefixes";
import { createStoragePath, formatDate, sanitizeTitleForFileSystem } from "@/utils/sanitize";
import { getSteps } from "./steps-utils";
import Steps from "./Steps";

export const createItem = (
  asset: 'video' | 'audio' | 'captions', 
  fn: Function.Function,
  query: StepperQuery,
  predicateFn: Function.Function) => 
    (data: StepperData, code?: string) => ({
      status: "waiting",
      createdAt: new Date().toISOString(),
      title: sanitizeTitleForFileSystem(data.meta.data.title),
      type: asset,
      storagePath: fn(asset, data.meta.data.title, code),
      url: query.url,
      // @ts-ignore
      ...data[asset].list.find(predicateFn),
    })

function FormStepper() {
  const globalCtx = React.useContext(AssetSubmitContext);
  const { setData, data, query, setQuery } = React.useContext(stepperContext);
  const { queue, setQueue } = React.useContext(QueueContext);
  const { settings } = React.useContext(SettingsContext);
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep(
      (prevActiveStep) => prevActiveStep + 1
    );
  };

  const handleSubmit = async () => {
    const parentUuid = formatDate(new Date())
    const fn = createStoragePath(settings.directory || ".", parentUuid)
    const audioItems = query.audioItags.map((itag) => createItem('audio', fn, query, (audio: StepperData["audio"]["list"][0]) => audio.itag === itag)(data))
    const videoItems = query.videoItags.map((itag) => createItem('video', fn, query, (video: StepperData["video"]["list"][0]) => video.itag === itag)(data))
    const captionItems = query.captions.map((code) => createItem('captions', fn, query, (caption: StepperData["captions"]["list"][0]) => caption.code === code)(data, code))

    await Promise.all([
      audioItems.map((item) => {
        SetItem(Prefix.QUEUE, JSON.stringify(item))
      }),
      videoItems.map((item) => {
        SetItem(Prefix.QUEUE, JSON.stringify(item))
      }),
      captionItems.map((item) => {
        SetItem(Prefix.QUEUE, JSON.stringify(item))
      }),
    ])

    setQueue([
      ...queue,
      ...audioItems,
      ...videoItems,
      ...captionItems,
    ])

    globalCtx.setData(data)
    globalCtx.setOpen(true)

    setActiveStep(
      (prevActiveStep) => prevActiveStep + 1
    );

    setQuery({
      ...query,
      url: "",
    })

    setTimeout(() => {
      setData(DEFAULT_DATA)
      setActiveStep(0)
    }, 500)
  }

  const handleBack = () => {
    setActiveStep(
      (prevActiveStep) => prevActiveStep - 1
    );
  };

  return (
    <>
      <Stepper
        activeStep={activeStep}
        orientation="vertical"
      >
        {steps.map((step, index) => (
          <Step>
            <StepLabel>{step.title}</StepLabel>
            <StepContent>
              <Card className="padding16 marginTopBottom16">
                <CardContent>
                  <Steps step={index} />
                </CardContent>
                <CardActions>
                  <Button
                    disabled={
                      activeStep === 0
                    }
                    onClick={handleBack}
                  >
                    Back
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={activeStep === 0 && !data.meta.data.title}
                    onClick={activeStep ===
                      steps.length - 1
                      ? handleSubmit
                      : handleNext}
                  >
                    {activeStep ===
                      steps.length - 1
                      ? "Finish"
                      : "Next"}
                  </Button>
                </CardActions>
              </Card>
            </StepContent>
          </Step>
        ))}
      </Stepper>
    </>
  );
}

export default withRoot(FormStepper);