import { useContext } from 'react'
import { FormControl, FormControlLabel, Checkbox, Alert } from '@mui/material'
import withRoot from '@/theme/withRoot'
import { stepperContext } from '@/context/stepper.context'

function CaptionsCheckBox() {
  const { data, query, setQuery } = useContext(stepperContext)

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setQuery({
      ...query,
      captions: [
        ...query.captions,
        event.target.name
      ]
    });
  };

  if (data.captions.error) {
    return (
      <Alert severity="error" sx={{ width: '100%' }}>
        {data.captions.error}
      </Alert>
    )
  }

  if (!data.captions.success) {
    return (
      <Alert severity="error" sx={{ width: '100%' }}>
        {data.captions.error}
      </Alert>
    )
  }

  if (data.captions.list.length === 0) { 
    return (
      <Alert severity="info" sx={{ width: '100%' }}>
        No captions available for this video.
      </Alert>
    )
  }

  return (
    <FormControl className='form-control'>
      {data.captions?.list.map((asset: Record<string, string>) => (
        <FormControlLabel
          control={
            <Checkbox checked={query.captions.includes(asset.code)} onChange={handleChange} name={asset.code} />
          }
          label={asset.lang}
        />
      ))}
    </FormControl>
  )
}

export default withRoot(CaptionsCheckBox)
