import { useCallback, useContext, useState } from 'react'
import { Alert, CircularProgress, FormControl, Grid, TextField } from '@mui/material'
import withRoot from '@/theme/withRoot'
import { DEFAULT_ERR_MSG, stepperContext } from '@/context/stepper.context';
import { GetVideoInfo, ListAudioAssets, ListCaptionsAssets, ListVideoAssets } from '@wails/go/main/App';

const isValidUrl = (url: string) => {
  try {
    const parsedUrl = new URL(url);
    return parsedUrl.hostname.includes("youtube.com") || parsedUrl.hostname.includes("youtu.be");
  } catch (e) {
    return false;
  }
};

function SelectYouTubeVideo() {
  const { query, setQuery, data, setData } = useContext(stepperContext)
  const [validUrl, setValidUrl] = useState(query.url.length > 0);
  const [ loading, setLoading ] = useState(false)
  const [url, setUrl] = useState(query.url);
  const apiFunction = useCallback((newUrl: string) => 
    Promise.all([
      GetVideoInfo(newUrl),
      ListVideoAssets(newUrl),
      ListAudioAssets(newUrl),
      ListCaptionsAssets(newUrl)
    ]), [query.url]
  )

  const handleUrlChange = async (event: { target: { value: any } }) => {
    const newUrl = event.target.value;
    setUrl(newUrl);

    if (!isValidUrl(newUrl)) {
      setValidUrl(false);
      return
    }

    setValidUrl(true)
    setLoading(true)
    return apiFunction(newUrl).then(
      meta => {
        setData({
          audio: JSON.parse(meta[2]),
          video: JSON.parse(meta[1]),
          captions: JSON.parse(meta[3]),
          meta: JSON.parse(meta[0]),
        })

        setQuery({
          ...query,
          url: newUrl,
        })
      }
    ).catch(console.error).finally(() => setLoading(false));
  };

  return (
    <FormControl className='form-control'>
      <Grid container spacing={2}>
        <Grid item xs={10}>
          <TextField
            fullWidth
            label="Type a YouTube URL"
            variant="outlined"
            value={url}
            onChange={handleUrlChange}
            error={!validUrl && url.length > 0}
            helperText={!validUrl && url.length > 0 ? "Not a YouTube URL" : ""}
          />
        </Grid>
        <Grid item xs={2}>
          {loading && <CircularProgress />}
        </Grid>
        <Grid item xs={12}>
          {data.audio.error && data.audio.error !== DEFAULT_ERR_MSG && (
            <Alert severity="error" sx={{ width: '100%' }}>
              {data.audio.error}
            </Alert>
          ) || data.video.error && data.video.error !== DEFAULT_ERR_MSG && (
            <Alert severity="error" sx={{ width: '100%' }}>
              {data.video.error}
            </Alert>
            ) || data.captions.error && data.captions.error !== DEFAULT_ERR_MSG && (
            <Alert severity="error" sx={{ width: '100%' }}>
              {data.captions.error}
            </Alert>
          )}
        </Grid>
      </Grid>
    </FormControl>
  )
}

export default withRoot(SelectYouTubeVideo)
