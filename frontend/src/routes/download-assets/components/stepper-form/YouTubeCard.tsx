import { useContext } from "react"
import { Card, CardContent, CardHeader, CardMedia, Typography } from "@mui/material"
import withRoot from "@/theme/withRoot"
import { stepperContext } from "@/context/stepper.context"

function YouTubeCard() {
  const { data } = useContext(stepperContext)

  if (!data.meta.success) {
    return null
  }

  return (
    <Card>
      <CardHeader
        title={data.meta.data.author}
        subheader={data.meta.data.publish_date}
      />
      <CardMedia
        sx={{ height: 140 }}
        image={data.meta.data.image}
        title={data.meta.data.title}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {data.meta.data.title}
        </Typography>
      </CardContent>
    </Card>
  )
}

export default withRoot(YouTubeCard)