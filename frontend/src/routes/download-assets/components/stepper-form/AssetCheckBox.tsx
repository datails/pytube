import { useContext } from 'react'
import type { Function } from "ts-toolbelt"
import { FormControl, FormControlLabel, Checkbox, Alert } from '@mui/material'

import withRoot from '@/theme/withRoot'
import { stepperContext } from '@/context/stepper.context'

export enum AssetType { 
  Video = 'video',
  Audio = 'audio',
}

interface Props {
  type: AssetType
}

const toAssetContextName = (type: AssetType): `${AssetType}Itags` => `${type}Itags`

function AssetCheckBox({ type }: Props & {
  fn: Function.Function
}) {
  const { data, query, setQuery } = useContext(stepperContext)
  const asset = data[type]

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const itag = parseInt(event.target.name)

    if (query[toAssetContextName(type)].includes(itag)) {
      setQuery({
        ...query,
        [toAssetContextName(type)]: query[toAssetContextName(type)].filter((itag) => itag !== parseInt(event.target.name)),
      });
      return
    }

    setQuery({
      ...query,
      [toAssetContextName(type)]: [
        ...query[toAssetContextName(type)] || [],
        parseInt(event.target.name)
      ],
    });
  };

  if (asset.error || !asset.success) {
    return (
      <Alert severity="error" sx={{ width: '100%' }}>
        {asset.error}
      </Alert>
    )
  }

  if (asset.list.length === 0) {
    return (
      <Alert severity="info" sx={{ width: '100%' }}>
        No {type} available for this video.
      </Alert>
    )
  }

  return (
    <FormControl className='form-control'>
      {asset?.list.map((asset) => (
        <FormControlLabel
          control={
            <Checkbox checked={query[toAssetContextName(type)].includes(asset.itag)} onChange={handleChange} name={asset.itag.toString()} />
          }
          label={`${asset.mime_type} ${'resolution' in asset ? asset.resolution : asset.abr}`}
        />
      ))}
    </FormControl>
  )
}

export default withRoot(AssetCheckBox)
