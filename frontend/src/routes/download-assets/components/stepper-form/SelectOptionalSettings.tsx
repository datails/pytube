import { useState } from 'react'
import { FormControl, FormControlLabel, Checkbox } from '@mui/material'
import withRoot from '@/theme/withRoot'

function SelectOptionalSettings() {
  const [state, setState] = useState({
    useFFmpeg: false,
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({
      ...state,
      [event.target.name]: event.target.checked,
    });
  };

  return (
    <FormControl className='form-control'>
      <FormControlLabel
        control={
          <Checkbox checked={state.useFFmpeg} onChange={handleChange} name={"useFFmpeg"} />
        }
        label={`Use FFmpeg`}
      />
    </FormControl>
  )
}

export default withRoot(SelectOptionalSettings)
