import withRoot from "@/theme/withRoot"
import SelectYouTubeVideo from "./SelectYouTubeVideo"
import AssetCheckBox, { AssetType } from "./AssetCheckBox"
import { Alert } from "@mui/material"
import SelectCaptions from "./SelectCaptions"

interface Props {
  step: number
}

function Steps({ step }: Props) {
  if (step === 0) { 
    return <SelectYouTubeVideo />
  }

  if (step === 1) { 
    return <AssetCheckBox type={AssetType.Audio} />
  }

  if (step === 2) { 
    return <AssetCheckBox type={AssetType.Video} />
  }

  if (step === 3) {
    return <SelectCaptions />
  }

  return (
    <Alert severity="error" sx={{ width: '100%' }}>
      Unknown step
    </Alert>
  )
}

export default withRoot(Steps)