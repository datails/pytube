interface Step { 
  title: string
}

export const getSteps = (): Step[] => [
  { title: 'YouTube URL' }, 
  { title: 'Select Audio Assets' },
  { title: 'Select Video Assets' }, 
  { title: 'Select captions' },
]
