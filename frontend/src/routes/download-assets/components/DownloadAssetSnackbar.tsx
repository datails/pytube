import {
  Card,
  CardHeader,
  Snackbar,
} from "@mui/material";
import withRoot from "@/theme/withRoot";
import { useContext } from "react";
import { AssetSubmitContext } from "@/context/app.context";
import { DEFAULT_DATA } from "@/context/stepper.context";

function DownloadAssetSnackbar() {
  const { open, setOpen, data, setData } = useContext(AssetSubmitContext)

  const handleClose = () => {
    setOpen(!open);
    setData(DEFAULT_DATA)
  };

  if (!data.meta.success) { 
    return null
  }
  
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right'
       }}
        autoHideDuration={6000}
        open={open}
        onClose={handleClose}
        key={data.meta.data.title}
      >
      <Card>
        <CardHeader
          title="Adding to queue..."
          subheader={data.meta.data.title}
        />
      </Card>
    </Snackbar>
  );
}

export default withRoot(DownloadAssetSnackbar);