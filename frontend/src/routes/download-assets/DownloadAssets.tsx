import { Grid, Typography } from '@mui/material'
import withRoot from '@/theme/withRoot'
import StepperForm from './components/stepper-form/StepperForm'
import { StepperContextProvider } from '@/context/stepper.context'
import YouTubeCard from './components/stepper-form/YouTubeCard'
import DownloadAssetSnackbar from './components/DownloadAssetSnackbar'

function DownloadAssets() {
  return (
    <Grid container spacing={2} id='app' justifyItems='center'>
      <Grid item xs={12} flexDirection='column'>
        <Typography className='margin16 marginBottom32' gutterBottom variant='h1'>
          Select assets to download from YouTube.
        </Typography>
      </Grid>
      <StepperContextProvider>
        <Grid item xs={8} flexDirection='column'>
          <StepperForm />
        </Grid>
        <Grid item xs={4} flexDirection='column'>
          <YouTubeCard />
        </Grid>
      </StepperContextProvider>
      <DownloadAssetSnackbar />
    </Grid>
  )
}

export default withRoot(DownloadAssets)
