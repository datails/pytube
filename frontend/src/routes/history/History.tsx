import { Card, CardContent, Grid, Typography } from '@mui/material'
import withRoot from '@/theme/withRoot'
import DataGrid from './components/DataGrid'

function History() {
  return (
    <Grid container spacing={2} id='app' justifyContent='center'>
      <Grid item xs={12} flexDirection='column' className='title-bar'>
        <Card>
          <CardContent>
            <Typography className='margin16 marginBottom32' gutterBottom variant='h1'>
              Queue
            </Typography>
            <Typography className='title marginBottom32' variant='h5'>
              Queue of assets to download
            </Typography>
            <DataGrid />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default withRoot(History)
