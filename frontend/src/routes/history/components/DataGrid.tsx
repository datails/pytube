import { Grid } from '@mui/material'
import withRoot from '@/theme/withRoot'
import { queueColumns } from '@/constants/columns'
import { useContext } from 'react'
import { QueueContext } from '@/context/app.context'
import DataTable from '@/components/data-table/DataTable'

function DataGrid() {
  const { queue } = useContext(QueueContext)
  
  return (
    <Grid container spacing={2} justifyContent='center'>
      <Grid item xs={12} flexDirection='column' className='title-bar'>
        <DataTable columns={queueColumns} rows={queue}></DataTable>
      </Grid>
    </Grid>
  )
} 

export default withRoot(DataGrid)
