export const sanitizeTitleForFileSystem = (title: string) => {
  // Remove emojis and special characters using a regular expression
  let sanitizedTitle = title.trim().replace(/[\u{1F600}-\u{1F64F}\u{1F300}-\u{1F5FF}\u{1F680}-\u{1F6FF}\u{2600}-\u{26FF}\u{2700}-\u{27BF}]/gu, '');

  sanitizedTitle = sanitizedTitle.replace(
    /([\u2700-\u27BF]|[\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2011-\u26FF]|\uD83E[\uDD10-\uDDFF])/g,
    ''
  )

  // Replace or remove characters not allowed in file names
  sanitizedTitle = sanitizedTitle.replace(/[\/\\:*?"<>|]/g, '-');

  // Replace all spaces and other whitespace characters with underscores
  sanitizedTitle = sanitizedTitle.replace(/\s+/g, '_');

  return sanitizedTitle.toLowerCase();
}

export const createStoragePath = (dir: string, parentUuid: string) => (asset: 'video' | 'audio' | 'captions', title: string, code?: string) => {
  const newDir = `${dir}/${parentUuid}/${sanitizeTitleForFileSystem(title)}/${asset}`

  if (asset !== 'captions') {
    return newDir
  }

  return `${newDir}/${code}`
}

export const formatDate = (date: Date): string => {
  let day = date.getDate().toString();
  let month = (date.getMonth() + 1).toString(); // January is 0!
  let year = date.getFullYear().toString();

  // Add leading zero to day and month if necessary
  day = day.length < 2 ? '0' + day : day;
  month = month.length < 2 ? '0' + month : month;

  return `${day}-${month}-${year}`;
};

export function getParentDirectory(filePath: string) {
  // Remove trailing slashes (if any)
  let normalizedPath = filePath.replace(/\/$/, '');

  // Find the last occurrence of a slash
  let lastSlashIndex = normalizedPath.lastIndexOf('/');

  // If there's no slash, return an empty string or '.' to indicate the current directory
  if (lastSlashIndex === -1) return '.';

  // Strip everything after the last slash
  normalizedPath = normalizedPath.substring(0, lastSlashIndex);

  // Find the second last slash
  lastSlashIndex = normalizedPath.lastIndexOf('/');

  // Return the parent directory
  // If there's no second slash, return root ('/') or '.' for relative paths
  return lastSlashIndex === -1 ? (normalizedPath.startsWith('/') ? '/' : '.') : normalizedPath.substring(0, lastSlashIndex);
}