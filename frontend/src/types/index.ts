export interface Rule {
  name: string
  id: string
  rules: string[]
}

export type SetFunction = (...props: unknown[]) => void | Promise<void>
