import { useCallback, useEffect, useMemo, useReducer, useRef } from 'react'

export interface State<T> {
  data?: T
  error?: Error
}

export type Action<T> =
  | { type: 'loading' }
  | { type: 'fetched'; payload: T }
  | { type: 'error'; payload: Error }

export type ApiFunction = (...args: any[]) => Promise<any>

const createInitialState = <T>(): State<T> => ({
  error: undefined,
  data: undefined,
})

const apiReducer = <T>(state: State<T>, action: Action<T>): State<T> => {
  switch (action.type) {
    case 'loading':
      return { ...createInitialState() }
    case 'fetched':
      return { ...createInitialState(), data: action.payload }
    case 'error':
      return { ...createInitialState(), error: action.payload }
    default:
      return state
  }
}

export const useApi = <T = any>(fn: ApiFunction, ...args: any[]): State<T> => {
  const [state, dispatch] = useReducer(apiReducer, createInitialState())
  const cancelRequest = useRef<boolean>(false)

  const memoizedArgs = useMemo(() => args, args)
  const apiFunction = useCallback(() => fn(...memoizedArgs), [fn, memoizedArgs])

  useEffect(() => {
    cancelRequest.current = false

    const fetchData = async () => {
      dispatch({ type: 'loading' })

      apiFunction().then(e => {
        dispatch({ type: 'fetched', payload: e })
      }).catch(err => {
        if (cancelRequest.current) return

        dispatch({ type: 'error', payload: err as Error })
      })
    }

    void fetchData()

    // Use the cleanup function for avoiding a possibly...
    // ...state update after the component was unmounted
    return () => {
      cancelRequest.current = true
    }
  }, [apiFunction])

  return state as State<T>
}
