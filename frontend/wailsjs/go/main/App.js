// @ts-check
// Cynhyrchwyd y ffeil hon yn awtomatig. PEIDIWCH Â MODIWL
// This file is automatically generated. DO NOT EDIT

export function ConvertMP4ToMP3(arg1, arg2) {
  return window['go']['main']['App']['ConvertMP4ToMP3'](arg1, arg2);
}

export function Delete(arg1) {
  return window['go']['main']['App']['Delete'](arg1);
}

export function DownloadAudioByItag(arg1, arg2, arg3, arg4) {
  return window['go']['main']['App']['DownloadAudioByItag'](arg1, arg2, arg3, arg4);
}

export function DownloadCaptionsByCode(arg1, arg2, arg3) {
  return window['go']['main']['App']['DownloadCaptionsByCode'](arg1, arg2, arg3);
}

export function DownloadVideoByItag(arg1, arg2, arg3, arg4) {
  return window['go']['main']['App']['DownloadVideoByItag'](arg1, arg2, arg3, arg4);
}

export function ExtractMetadata(arg1) {
  return window['go']['main']['App']['ExtractMetadata'](arg1);
}

export function Get(arg1) {
  return window['go']['main']['App']['Get'](arg1);
}

export function GetVideoInfo(arg1) {
  return window['go']['main']['App']['GetVideoInfo'](arg1);
}

export function Greet(arg1) {
  return window['go']['main']['App']['Greet'](arg1);
}

export function HandleQueue() {
  return window['go']['main']['App']['HandleQueue']();
}

export function List(arg1) {
  return window['go']['main']['App']['List'](arg1);
}

export function ListAudioAssets(arg1) {
  return window['go']['main']['App']['ListAudioAssets'](arg1);
}

export function ListCaptionsAssets(arg1) {
  return window['go']['main']['App']['ListCaptionsAssets'](arg1);
}

export function ListVideoAssets(arg1) {
  return window['go']['main']['App']['ListVideoAssets'](arg1);
}

export function Reset() {
  return window['go']['main']['App']['Reset']();
}

export function ResetPrefix(arg1) {
  return window['go']['main']['App']['ResetPrefix'](arg1);
}

export function SelectDir() {
  return window['go']['main']['App']['SelectDir']();
}

export function SelectFile() {
  return window['go']['main']['App']['SelectFile']();
}

export function Set(arg1, arg2) {
  return window['go']['main']['App']['Set'](arg1, arg2);
}

export function SetItem(arg1, arg2) {
  return window['go']['main']['App']['SetItem'](arg1, arg2);
}

export function SynchronizeAudioAndVideo(arg1, arg2, arg3) {
  return window['go']['main']['App']['SynchronizeAudioAndVideo'](arg1, arg2, arg3);
}
