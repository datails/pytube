import os
from pytubefix import YouTube
import json
import sys

def get_file_extension(mime_type):
    # Extracts file extension from mime type
    return mime_type.split('/')[-1]

def list_videos(url):
    try:
        yt = YouTube(url)
        yt.bypass_age_gate()
        video_streams = yt.streams.filter(only_video=True)

        # Convert each video stream to a dictionary
        streams_info = []
        for stream in video_streams:
            stream_info = {
                'itag': stream.itag,
                'mime_type': stream.mime_type,
                'resolution': stream.resolution,
                'fps': stream.fps,
                'vcodec': stream.video_codec,
                'progressive': stream.is_progressive,
                'type': stream.type
            }
            streams_info.append(stream_info)

        result = {'success': True, 'list': streams_info}
    except Exception as e:
        result = {'success': False, 'error': str(e)}

    # convert to a stringified JSON object
    print(json.dumps(result))
    
def list_captions(url):
    try:
        yt = YouTube(url)
        yt.bypass_age_gate()
        captions = yt.caption_tracks
        captionList = []

        # loop over captions
        for caption in captions:
            captionList.append({
                'code': caption.code,
                'lang': caption.name
            })

        result = {'success': True, 'list': captionList}
    except Exception as e:
        result = {'success': False, 'error': str(e)}
    print(json.dumps(result))

def list_audio(url):
    try:
        yt = YouTube(url)
        yt.bypass_age_gate()
        audio_streams = yt.streams.filter(only_audio=True)

        # Convert each video stream to a dictionary
        streams_info = []
        for stream in audio_streams:
            stream_info = {
                'itag': stream.itag,
                'mime_type': stream.mime_type,
                'abr': stream.abr,
                'acodec': stream.audio_codec,
                'progressive': stream.is_progressive,
                'type': stream.type
            }
            streams_info.append(stream_info)

        result = {'success': True, 'list': streams_info}
    except Exception as e:
        result = {'success': False, 'error': str(e)}

    # Convert the result to JSON and return it
    print(json.dumps(result))

def download_video_by_itag(url, itag, file_name, output_dir):
    try:
        yt = YouTube(url)
        yt.bypass_age_gate()

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # Download the best video (without audio) and best audio separately
        video_stream = yt.streams.get_by_itag(itag)

        video_ext = get_file_extension(video_stream.mime_type)

        video_file_path = video_stream.download(output_path=output_dir, filename=f"{file_name}_video.{video_ext}")

        result = {'success': True, 'filename': video_file_path}
    except Exception as e:
        result = {'success': False, 'error': str(e)}

    # Convert the result to JSON and print it so that JavaScript can read it
    print(json.dumps(result))

def download_audio_by_itag(url, itag, file_name, output_dir):
    try:
        yt = YouTube(url)
        yt.bypass_age_gate()

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # Download the best video (without audio) and best audio separately
        audio_stream = yt.streams.get_by_itag(itag)

        audio_ext = get_file_extension(audio_stream.mime_type)

        audio_file_path = audio_stream.download(output_path=output_dir, filename=f"{file_name}_audio.{audio_ext}")

        result = {'success': True, 'filename': audio_file_path}
    except Exception as e:
        result = {'success': False, 'error': str(e)}

    # Convert the result to JSON and print it so that JavaScript can read it
    print(json.dumps(result))   
 
def download_captions_by_code(url, code, output_dir):
    try:
        yt = YouTube(url)
        yt.bypass_age_gate()
        video_title = yt.title.replace(" ", "_")

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # Download the caption in SRT format
        if code in yt.captions:
            caption = yt.captions[code]
            # print(caption.json_captions)
            srt_caption_file_path = caption.download(output_path=output_dir, srt=True, title=f"{video_title}_caption")
            result = {'success': True, 'srt_filename': srt_caption_file_path}
        else:
            result = {'success': False, 'error': f'Caption with code {code} not found'}
    except Exception as e:
        import traceback
        traceback.print_exc()
        result = {'success': False, 'error': f'An error occurred: {str(e)}'}

    # Convert the result to JSON and print it
    print(json.dumps(result))


def get_video_info(url):
    try:
        yt = YouTube(url)
        yt.bypass_age_gate()
        result = {
            'success': True,
            'data': {
                'image': yt.thumbnail_url, 
                'title': yt.title, 
                'description': yt.description, 
                'author': yt.author, 
                'length': yt.length,
                'publish_date': yt.publish_date.strftime("%Y-%m-%d %H:%M:%S")
            }
        }
    except Exception as e:
        result = {'success': False, 'error': str(e)}

    print(json.dumps(result))

# def download_playlist(url, output_path):
#     playlist = Playlist(url)
#     for video_url in playlist.video_urls:
#         download_video(video_url, output_path)

functions = {
    'list_videos': list_videos,
    'list_captions': list_captions,
    'list_audio': list_audio,
    'download_video_by_itag': download_video_by_itag,
    'download_audio_by_itag': download_audio_by_itag,
    'download_captions_by_code': download_captions_by_code,
    'get_video_info': get_video_info,
    # 'download_playlist': download_playlist,
}

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: main.py <function_to_execute> [arguments]")
    else:
        function_to_execute = sys.argv[1]
        if function_to_execute in functions:
            # Call the function with the remaining arguments
            functions[function_to_execute](*sys.argv[2:])
        else:
            print(f"Function '{function_to_execute}' not recognized.")