package main

import (
	"context"
	"embed"
	"log"

	"github.com/robfig/cron/v3"
	"github.com/wailsapp/wails/v2"
	"github.com/wailsapp/wails/v2/pkg/options"
	"github.com/wailsapp/wails/v2/pkg/options/assetserver"
)

//go:embed all:frontend/dist
var assets embed.FS

func main() {
	cr := cron.New()
	app := NewApp()
	err := InitDB()

	if err != nil {
		log.Fatalf("Failed to initialize the database: %v", err)
	}

	defer CloseDB()
	defer cr.Stop()

	// Create application with options
	err = wails.Run(&options.App{
		Title:  "pytube",
		Width:  1024,
		Height: 768,
		AssetServer: &assetserver.Options{
			Assets: assets,
		},
		BackgroundColour: &options.RGBA{R: 255, G: 255, B: 255, A: 1},
		OnStartup: func(ctx context.Context) {
			app.startup(ctx) // Pass the context to your app
			cr.AddFunc("@every 5s", app.HandleQueue)
			cr.Start()
		},
		OnShutdown: func(ctx context.Context) {
			CloseDB()
			cr.Stop()
		},
		Bind: []interface{}{
			app,
		},
	})

	if err != nil {
		println("Error:", err.Error())
	}
}
