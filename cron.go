package main

import (
	"encoding/json"
	"errors"
)

var record map[string]interface{}

func findFirstWaitingItem(jsonArray []string) (*map[string]interface{}, error) {
	var earliestItem *map[string]interface{}
	var earliestDate string

	for _, strObj := range jsonArray {
		var obj map[string]interface{}
		err := json.Unmarshal([]byte(strObj), &obj)
		if err != nil {
			continue
		}

		if valueStr, ok := obj["value"].(string); ok {
			var valueObj map[string]interface{}
			err := json.Unmarshal([]byte(valueStr), &valueObj)
			if err != nil {
				continue
			}

			if status, ok := valueObj["status"].(string); ok && status == "waiting" {
				createdAt, ok := valueObj["createdAt"].(string) // Assuming createdAt is a string
				if !ok {
					continue
				}

				// Compare dates and update earliestItem if current item is earlier
				if earliestItem == nil || createdAt < earliestDate {
					earliestItem = &obj
					earliestDate = createdAt
				}
			}
		}
	}

	if earliestItem != nil {
		return earliestItem, nil
	} else {
		return nil, errors.New("no item with status 'waiting' found")
	}
}

func updateJSONValue(jsonStr string, key string, value interface{}) (string, error) {
	var obj map[string]interface{}
	err := json.Unmarshal([]byte(jsonStr), &obj)
	if err != nil {
		return "", err
	}

	obj[key] = value

	modifiedValue, err := json.Marshal(obj)
	if err != nil {
		return "", err
	}

	return string(modifiedValue), nil
}

func (a *App) HandleQueue() {
	jsonArray, err := a.List("queue")
	if err != nil {
		println("Error:", err.Error())
		return
	}

	item, err := findFirstWaitingItem(jsonArray)

	if err != nil {
		return
	}

	if id, ok := (*item)["key"].(string); ok {
		if value, ok := (*item)["value"].(string); ok {
			// Unmarshal value into a map
			var valueMap map[string]interface{}
			err := json.Unmarshal([]byte(value), &valueMap)
			if err != nil {
				println("Error unmarshalling value:", err.Error())
				return
			}

			modifiedValue, _ := updateJSONValue(value, "status", "downloading...")

			// Set the modified value
			err = a.Set(id, modifiedValue)

			if err != nil {
				println("Error setting modified value:", err.Error())
				return
			}

			if valueMap["type"] == "video" {
				if url, ok := valueMap["url"].(string); ok {
					if fileName, ok := valueMap["title"].(string); ok {
						if itag, ok := valueMap["itag"].(float64); ok {
							if outputPath, ok := valueMap["storagePath"].(string); ok {
								_, err := a.DownloadVideoByItag(url, itag, fileName, outputPath)
								if err != nil {
									mod, _ := updateJSONValue(value, "status", "failed")
									modifiedValue, _ := updateJSONValue(mod, "retry", 1)
									err = a.Set(id, modifiedValue)
									return
								}

								modifiedValue, _ := updateJSONValue(value, "status", "ready")
								err = a.Set(id, modifiedValue)
								return
							}
						}
					}
				}
			} else if valueMap["type"] == "audio" {
				if url, ok := valueMap["url"].(string); ok {
					if fileName, ok := valueMap["title"].(string); ok {
						if itag, ok := valueMap["itag"].(float64); ok {
							if outputPath, ok := valueMap["storagePath"].(string); ok {
								_, err := a.DownloadAudioByItag(url, itag, fileName, outputPath)
								if err != nil {
									mod, _ := updateJSONValue(value, "status", "failed")
									modifiedValue, _ := updateJSONValue(mod, "retry", 1)
									err = a.Set(id, modifiedValue)
									return
								}

								modifiedValue, _ := updateJSONValue(value, "status", "ready")
								err = a.Set(id, modifiedValue)
								return
							}
						}
					}
				}
			} else if valueMap["type"] == "captions" {
				if url, ok := valueMap["url"].(string); ok {
					if code, ok := valueMap["code"].(string); ok {
						if outputPath, ok := valueMap["storagePath"].(string); ok {
							_, err := a.DownloadCaptionsByCode(url, code, outputPath)
							if err != nil {
								mod, _ := updateJSONValue(value, "status", "failed")
								modifiedValue, _ := updateJSONValue(mod, "retry", 1)
								err = a.Set(id, modifiedValue)
								return
							}

							modifiedValue, _ := updateJSONValue(value, "status", "ready")
							err = a.Set(id, modifiedValue)
						}
					}
				}
			} else if valueMap["type"] == "ffmpeg" {
				if audioFile, ok := valueMap["audioFile"].(string); ok {
					if videoFile, ok := valueMap["videoFile"].(string); ok {
						if outputFile, ok := valueMap["outputFile"].(string); ok {
							err := a.SynchronizeAudioAndVideo(audioFile, videoFile, outputFile)
							if err != nil {
								mod, _ := updateJSONValue(value, "status", "failed")
								modifiedValue, _ := updateJSONValue(mod, "retry", 1)
								err = a.Set(id, modifiedValue)
								return
							}

							modifiedValue, _ := updateJSONValue(value, "status", "ready")
							err = a.Set(id, modifiedValue)
						}
					}
				}
			}
		}
	}
}
