!include "MUI2.nsh"
!include "nsUnzip.nsh"  ; Make sure you have the nsUnzip plugin

Name "PyTube"
OutFile "PyTubeInstaller.exe"

InstallDir $PROGRAMFILES\PyTube

RequestExecutionLevel admin

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "LICENCE"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

Section "Install Python and Application"
    SetOutPath $INSTDIR

    ; Check if Python is already installed
    ClearErrors
    ReadRegStr $0 HKLM "SOFTWARE\Python\PythonCore\3.10\InstallPath" ""
    IfErrors 0 PythonAlreadyInstalled

    ; Download Python installer
    NSISdl::download "https://www.python.org/ftp/python/3.10.0/python-3.10.0.exe" "$INSTDIR\python_installer.exe"
    Pop $0
    StrCmp $0 "success" +3
        MessageBox MB_ICONSTOP "Failed to download Python."
        Abort

    ; Install Python
    ExecWait '"$INSTDIR\python_installer.exe" /quiet InstallAllUsers=1 PrependPath=1'
    Delete "$INSTDIR\python_installer.exe"

    PythonAlreadyInstalled:

    ; Copy the scripts directory (including requirements.txt)
    SetOutPath $INSTDIR\scripts
    File /r "path\to\your\local\scripts\*.*"

    ; Check again if Python is installed and get the install path
    ClearErrors
    ReadRegStr $0 HKLM "SOFTWARE\Python\PythonCore\3.10\InstallPath" ""
    IfErrors PythonNotPresent
    StrCpy $1 "$0\python.exe"

    ; Run pip install
    ExecWait '"$1" -m pip3 install -r "$INSTDIR\scripts\requirements.txt"'

    PythonNotPresent:
SectionEnd

Section "Install FFmpeg"
    SetOutPath $INSTDIR

    ; Download FFmpeg
    NSISdl::download "https://ffmpeg.org/releases/ffmpeg-release-full.7z" "$INSTDIR\ffmpeg.7z"
    Pop $0
    StrCmp $0 "success" +3
        MessageBox MB_ICONSTOP "Failed to download FFmpeg."
        Abort

    ; Extract FFmpeg
    nsUnzip::Unzip "$INSTDIR\ffmpeg.7z" "$INSTDIR\ffmpeg"
    Pop $0
    StrCmp $0 "success" +2
        MessageBox MB_ICONSTOP "Failed to extract FFmpeg."

    ; Delete the downloaded archive
    Delete "$INSTDIR\ffmpeg.7z"

    ; Add FFmpeg to the system PATH
    ReadRegStr $0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path"
    StrCpy $1 "$0;$INSTDIR\ffmpeg\bin"
    WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path" "$1"

    ; Notify Windows that the PATH has changed
    SendMessage HWND_BROADCAST WM_SETTINGCHANGE 0 "STR:Environment" /TIMEOUT=5000
SectionEnd

Function .onInit
    ; Request Windows to refresh the environment variables
    System::Call 'kernel32::SetEnvironmentVariable(t "PATH", t 0)'
FunctionEnd
