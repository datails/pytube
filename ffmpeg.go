package main

import (
	"log"
	"os/exec"
)

func (a *App) ExtractMetadata(filePath string) (string, error) {
	cmd := exec.Command("ffprobe", "-v", "quiet", "-print_format", "json", "-show_format", "-show_streams", filePath)
	output, err := cmd.CombinedOutput()

	if err != nil {
		log.Printf("FFmpeg Error: %s\n", string(output))
		return "", err
	}

	return string(output), nil
}

func (a *App) SynchronizeAudioAndVideo(audioFile string, videoFile string, outputFile string) error {
	print("args", audioFile, videoFile, outputFile)
	cmd := exec.Command("ffmpeg", "-i", audioFile, "-i", videoFile, "-c:v", "copy", "-c:a", "aac", outputFile)
	output, err := cmd.CombinedOutput()

	if err != nil {
		log.Printf("FFmpeg Error: %s\n", string(output))
		return err
	}

	return nil
}

func (a *App) ConvertMP4ToMP3(inputFile string, outputFile string) (string, error) {
	cmd := exec.Command("ffmpeg", "-i", inputFile, "-vn", "-ab", "128k", "-ar", "44100", "-y", outputFile)
	output, err := cmd.CombinedOutput()

	if err != nil {
		log.Printf("FFmpeg Error: %s\n", string(output))
		return "", err
	}

	return string(output), nil
}
