package main

import (
	"embed"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
)

//go:embed scripts
var scripts embed.FS

const SCRIPTS = "scripts"
const PYTHON_FILE = "app.py"

func CreatePythonExecutor(pythonFn string) func(args ...string) (string, error) {
	return func(args ...string) (string, error) {
		// Extract the script to a temporary directory
		tempDir, err := os.MkdirTemp("", SCRIPTS)
		if err != nil {
			return "", err
		}
		defer os.RemoveAll(tempDir) // Clean up afterwards

		scriptFile, err := scripts.ReadFile(filepath.Join(SCRIPTS, PYTHON_FILE))
		if err != nil {
			return "", err
		}

		tempScriptPath := filepath.Join(tempDir, PYTHON_FILE)
		err = os.WriteFile(tempScriptPath, scriptFile, 0644)
		if err != nil {
			return "", err
		}

		// Prepare arguments for executing the script
		cmdArgs := append([]string{tempScriptPath, pythonFn}, args...)

		// Execute the script
		cmd := exec.Command("python3", cmdArgs...)
		output, err := cmd.CombinedOutput()

		if err != nil {
			return "", err
		}

		// return result of execution
		return string(output), nil

	}
}

func (a *App) GetVideoInfo(url string) (string, error) {
	return CreatePythonExecutor("get_video_info")(url)
}

func (a *App) ListVideoAssets(url string) (string, error) {
	return CreatePythonExecutor("list_videos")(url)
}

func (a *App) ListCaptionsAssets(url string) (string, error) {
	return CreatePythonExecutor("list_captions")(url)
}

func (a *App) ListAudioAssets(url string) (string, error) {
	return CreatePythonExecutor("list_audio")(url)
}

func (a *App) DownloadVideoByItag(url string, itag float64, fileName string, outputPath string) (string, error) {
	return CreatePythonExecutor("download_video_by_itag")(url, strconv.Itoa(int(itag)), fileName, outputPath)
}

func (a *App) DownloadAudioByItag(url string, itag float64, fileName string, outputPath string) (string, error) {
	return CreatePythonExecutor("download_audio_by_itag")(url, strconv.Itoa(int(itag)), fileName, outputPath)
}

func (a *App) DownloadCaptionsByCode(url string, code string, outputPath string) (string, error) {
	return CreatePythonExecutor("download_captions_by_code")(url, code, outputPath)
}
