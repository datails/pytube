# PyTube
Welcome to PyTube, a desktop application that brings the power of the Python library [PyTube](https://github.com/JuanBindez/pytubefix/tree/main/pytubefix) and [FFMpeg](https://ffmpeg.org/) into a user-friendly interface. `PyTube` is designed to facilitate the downloading and translating of YouTube videos with ease. This project is powered by [Wails](https://wails.io/), a framework for building desktop applications using `Go` & Web Technologies, and is developed using `React`.

## Features
* Download YouTube Assets: Easily download your favorite YouTube assets directly to your device.
* Synchronize: Synchronize an audio and video file with ease.
* User-Friendly Interface: A simple and intuitive UI, making video downloading and translating straightforward.
* Cross-Platform Support: Available for Windows and macOS users.

## Prerequisites
Before you begin, ensure you have the following installed:

* go (Golang) 1.21+
* python3
* ffmpeg
* node (Node.js) 18+

These are essential for running and building the PyTube application.

## Development Setup
To set up your development environment for PyTube, follow these steps:

1. Clone the repository:

```bash
git clone https://gitlab.com/datails/pytube
cd pytube
```

Start the development server:

```bash
wails dev
```

This will start the application in development mode.

## Building the Application
You can build PyTube for different platforms using the following commands:

### For Windows
**64-bit version:**

```bash
wails build -p -platform windows/amd64 -o pytube-amd64.exe -nsis
```

**ARM64 version:**

```bash
wails build -p -platform windows/arm64 -o pytube-arm64.exe -nsis
```

**For macOS Universal:**
```bash
wails build -p -platform darwin/universal
```

After building, the executables will be available in the specified output directory.

## Contributing
Contributions to PyTube are welcome! Please read our contributing guidelines to get started.

## License
PyTube is released under MIT. See the LICENSE file for more details.

## Acknowledgements
A special thanks to the creators and contributors of the PyTube library, FFMpeg, Wails, and React for making this project possible.