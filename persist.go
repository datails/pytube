package main

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/google/uuid"

	badger "github.com/dgraph-io/badger/v3"
)

const (
	STORAGE_PATH = "/tmp/badger"
)

var dbInstance *badger.DB

func InitDB() error {
	opts := badger.DefaultOptions(STORAGE_PATH).WithSyncWrites(true)
	var err error
	dbInstance, err = badger.Open(opts)
	return err
}

func CloseDB() {
	if dbInstance != nil {
		dbInstance.Close()
	}
}

type Rule struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func (a *App) SetItem(prefix string, value string) error {
	err := a.Set(prefix+uuid.New().String(), value)

	if err != nil {
		return err
	}

	return nil
}

func (a *App) Set(id string, value string) error {
	if dbInstance == nil {
		return errors.New("DB is not initialized")
	}

	err := dbInstance.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(id), []byte(value))
		return err
	})

	if err != nil {
		return err
	}

	return nil
}

func GetFromView(key string, valCopy *[]byte) func(txn *badger.Txn) error {
	return func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(key))

		if err != nil {
			return err
		}

		err = item.Value(func(val []byte) error {
			*valCopy = append([]byte{}, val...)
			return nil
		})

		if err != nil {
			return err
		}

		return nil
	}
}

func DelFromView(key string) func(txn *badger.Txn) error {
	return func(txn *badger.Txn) error {
		err := txn.Delete([]byte(key))

		if err != nil {
			return err
		}

		return nil
	}
}

func (a *App) Reset() error {
	if dbInstance == nil {
		return errors.New("DB is not initialized")
	}

	if err := dbInstance.DropAll(); err != nil {
		return err
	}

	return nil
}

func (a *App) ResetPrefix(prefix string) error {
	if dbInstance == nil {
		return errors.New("DB is not initialized")
	}

	if err := dbInstance.DropPrefix([]byte(prefix)); err != nil {
		return err
	}

	return nil
}

func (a *App) Delete(key string) error {
	if dbInstance == nil {
		return errors.New("DB is not initialized")
	}

	if err := dbInstance.Update(DelFromView(key)); err != nil {
		return err
	}

	return nil
}

func (a *App) Get(key string) (string, error) {
	if dbInstance == nil {
		return "", errors.New("DB is not initialized")
	}

	var valCopy []byte

	if err := dbInstance.View(GetFromView(key, &valCopy)); err != nil {
		return "", err
	}

	return string(valCopy), nil
}

func handleValue(key string, keyPair *[]string) func(v []byte) error {
	return func(v []byte) error {
		curr := &Rule{Key: key, Value: string(v)}

		b, err := json.Marshal(curr)

		if err != nil {
			fmt.Print("Something went wrong", errors.New((err.Error())))
		}

		*keyPair = append(*keyPair, string(b))
		return nil
	}
}

func Loop(keyPair *[]string, pref []byte) func(txn *badger.Txn) error {
	return func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()

		for it.Seek(pref); it.ValidForPrefix(pref); it.Next() {
			item := it.Item()
			k := item.Key()

			if err := item.Value(handleValue(string(k), keyPair)); err != nil {
				return err
			}
		}

		return nil
	}
}

func (a *App) List(prefix string) ([]string, error) {
	if dbInstance == nil {
		return nil, errors.New("DB is not initialized")
	}

	keyPair := &[]string{}
	pref := []byte(prefix)

	err := dbInstance.View(Loop(keyPair, pref))

	if err != nil {
		return nil, err
	}

	return *keyPair, nil
}
